import {Route as BrowserRoute, Routes} from 'react-router-dom'
import React from "react";
import {DedicatedWorker} from "./page/dedicatedWorker";
import {SimpleWorker} from "./page/simpleWorker";
import {SharedWorkerView} from "./page/sharedWorker";
import {PaintWorklet} from "./page/paintWorklet";
import {OtherPage, ScenarioWorker} from "./page/scenario";

export default function Router() {

    const routes = [
        { path: '/', element: <SimpleWorker /> },
        { path: '/dedicated-worker', element: <DedicatedWorker /> },
        { path: '/shared-worker', element: <SharedWorkerView /> },
        { path: '/worklet', element: <PaintWorklet /> },
        { path: '/scenario', element: <ScenarioWorker /> },
        { path: '/other_page', element: <OtherPage /> },
    ]

    return (
            <Routes>
                { routes.map((route) => (
                     <BrowserRoute key={route.path} path={route.path} element={route.element} />
                ))
                }
            </Routes>
        )
}

