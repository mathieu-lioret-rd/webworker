import React, {FormEvent, useEffect, useRef, useState} from "react";
import {Button, Form, Input, Title} from "../style";


// WITH Webpack 5 it is works !
// @see https://webpack.js.org/guides/web-workers/
export const dedicatedWorker = new Worker(new URL('./worker.ts', import.meta.url))

export const DedicatedWorker = () => {
    const workerRef = useRef<Worker>()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [result, setResult] = useState<string>()

    useEffect(() => {
        workerRef.current = dedicatedWorker
        workerRef.current.onmessage = (event: any) => {
            setIsLoading(false)
            console.log('Read message from dedicated worker', event)
            setResult(event.data)
        }
    }, [])

    const onSubmit = (event: FormEvent) => {
        setResult('')
        setIsLoading(true)
        event.preventDefault()
        // @ts-ignore
        const message = event.target[0].value
        workerRef?.current?.postMessage(message)
    }

    return (
        <div>
            <Title>
                Dedicated Worker
            </Title>
            <Form onSubmit={onSubmit}>
                <label htmlFor="simple-worker-input">Enter your message</label>
                <Input id='simple-worker-input' type="text" required/>
                <Button type="submit">soumettre</Button>
            </Form>
            <div>
                <h3>Result</h3>
                <p>{ isLoading && 'is loading...'}</p>
                <p>{result}</p>
            </div>
        </div>
    )
}