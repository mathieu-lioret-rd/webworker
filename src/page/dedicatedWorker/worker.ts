// @ts-ignore
import {sleep} from "../../utils";

onmessage = (event) => {
    console.log('Start Dedicated Worker', event)
    console.log('on message on worker', event)
    sleep(2000).then(() => {
        console.log('post message on worker', event.data)
        if(event.data && event.data.length > 0){
            const reverseMessage = event.data.split("").reverse().join("");
            postMessage(reverseMessage)
        }
        console.log('End Dedicated Worker', event)
    })
}
