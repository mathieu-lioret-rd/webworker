// @ts-ignore
import {sleep} from "../../utils";

const allPorts: any[] = [];

// @ts-ignore
onconnect = (event: any) => {
    console.log('Start Shared Worker', event)
    console.log('on connect', event)
    const [port] = event.ports
    allPorts.push(port)

    port.onmessage = (eventMessage: any) => {
        console.log('on message on worker', eventMessage)
        sleep(2000).then(() => {
            allPorts.forEach((aPort) => {
                if(eventMessage.data && eventMessage.data.length > 0){
                    const reverseMessage = eventMessage.data.split("").reverse().join("");
                    aPort.postMessage(reverseMessage)
                }
            })
        })
        console.log('end on message on worker', eventMessage)
    }
    console.log('End Shared Worker', event)
}