import React, {FormEvent, useEffect, useRef, useState} from "react";
import {Button, Form, Input, Title} from "../style";

export const SharedWorkerView = () => {
    const workerRef = useRef<SharedWorker>()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [result, setResult] = useState<string>()

    const sharedWorker = new SharedWorker(new URL('./shared-worker.ts', import.meta.url))

    useEffect(() => {
        workerRef.current = sharedWorker
        if(!workerRef.current) {
            console.log('shared worker not defined')
            return
        }
        workerRef.current.port.start()
        workerRef.current.port.onmessage = (event: any) => {
            setIsLoading(false)
            console.log('Read message from shared worker', event)
            setResult(event.data)
        }
    }, [])

    const onSubmit = (event: FormEvent) => {
        event.preventDefault()
        setResult('')
        setIsLoading(true)
        // @ts-ignore
        const message = event.target[0].value
        if(!workerRef.current) {
            console.log('shared worker not defined')
            return
        }
        workerRef.current.port.postMessage(message)
    }

    return (
        <div>
            <Title>
                Shared Worker
            </Title>
            <Form onSubmit={onSubmit}>
                <label htmlFor="simple-worker-input">Enter your message</label>
                <Input id='simple-worker-input' type="text" required/>
                <Button type="submit">soumettre</Button>
            </Form>
            <div>
                <h3>Result</h3>
                <p>{ isLoading && 'is loading...'}</p>
                <p>{result}</p>
            </div>
        </div>
    )
}