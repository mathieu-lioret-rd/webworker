import styled from "styled-components"

export const Title = styled.h2`
  margin: 1rem;
`

export const Form = styled.form`
  justify-content: center;
  display: flex;
  flex-direction: column;
  width: 50%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 1rem;
  text-align: left;
`

export const Input = styled.input`
  margin-top: 0.5rem;
`

export const Button = styled.button`
  margin-top: 1rem;
  max-width: 10rem;
  border: 0;
  padding: 1rem;
  color: white;
  background-color: #1e90ff;
  border-radius: 1rem;
  font-size: 1rem;

  &:hover {
    background-color: #2a80d5;
  }
`