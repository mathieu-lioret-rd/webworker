import React, {FormEvent, useEffect, useRef, useState} from "react";
import {Button, Form, Input, Title} from "../style";
import {dedicatedWorker} from "./instanceWorker";
import {NavLink} from "react-router-dom";


export const ScenarioWorker = () => {
    const workerRef = useRef<Worker>()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [result, setResult] = useState<string>()

    useEffect(() => {
        workerRef.current = dedicatedWorker
        workerRef.current.onmessage = (event: any) => {
            setIsLoading(false)
            console.log('Read message from dedicated worker', event)
            setResult(event.data)
        }
    }, [])

    const onSubmit = (event: FormEvent) => {
        setResult('')
        setIsLoading(true)
        event.preventDefault()
        // @ts-ignore
        const message = event.target[0].value
        workerRef?.current?.postMessage(message)
    }

    return (
        <div>
            <Title>
                Scenario Worker
            </Title>
            <NavLink to='/other_page'>Other page</NavLink>
            <Form onSubmit={onSubmit}>
                <label htmlFor="simple-worker-input">Enter your message</label>
                <Input id='simple-worker-input' type="text" required/>
                <Button type="submit">soumettre</Button>
            </Form>
            <div>
                <h3>Result</h3>
                <p>{ isLoading && 'is loading...'}</p>
                <p>{result}</p>
            </div>
        </div>
    )
}

export const OtherPage = () => {
    return (
        <div>
            <Title>
                Other Page
            </Title>
            <NavLink to='/scenario'>Return to Back</NavLink>
        </div>
    )
}