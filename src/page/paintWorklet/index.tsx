import React, {useEffect, useState} from "react";
import {Input, Title} from "../style";
import './style.scss'

export const PaintWorklet = () => {
    const [isEnabled, setIsEnabled] = useState<boolean>(false)

    useEffect(() => {
        if ('paintWorklet' in CSS) {
            // @ts-ignore
            CSS.paintWorklet.addModule('/js/CheckerBoardPainter.js')
        }
    }, [])

    const onChange = (event: any) => {
        setIsEnabled((event.target as HTMLInputElement).checked)
    }

    return (
        <div>
            <Title>
                Paint Worklet
            </Title>
            <label htmlFor="simple-worker-input">Activate your paint worklet</label>
            <Input id='simple-worker-input' type="checkbox" required onChange={onChange}/>
            <h3>result</h3>
            {
                <div className={isEnabled ? 'container container--activated' : 'container'}/>
            }
        </div>
    )
}