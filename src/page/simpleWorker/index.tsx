import React, {FormEvent, useEffect, useRef, useState} from "react";
import {Button, Form, Input, Title} from "../style";


export const SimpleWorker = () => {
    const workerRef = useRef<Worker>()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [result, setResult] = useState<string>()

    useEffect(() => {
        workerRef.current = new Worker('./js/worker/simpleWorker.js')
        workerRef.current.onmessage = (event) => {
            setIsLoading(false)
            console.log('Read message from simple worker', event)
            setResult(event.data)
        }
        return () => {
            workerRef.current?.terminate()
        }
    }, [])

    const onSubmit = (event: FormEvent) => {
        setResult('')
        setIsLoading(true)
        event.preventDefault()
        // @ts-ignore
        const message = event.target[0].value
        workerRef?.current?.postMessage(message)
    }

    return (
        <div>
            <Title>
                Simple Worker
            </Title>
            <Form onSubmit={onSubmit}>
                <label htmlFor="simple-worker-input">Enter your message</label>
                <Input id='simple-worker-input' type="text" required/>
                <Button type="submit">soumettre</Button>
            </Form>
            <div>
                <h3>Result</h3>
                <p>{ isLoading && 'is loading...'}</p>
                <p>{result}</p>
            </div>
        </div>
    )
}