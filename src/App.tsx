import React from 'react';
import './App.css';
import Router from "./Router";
import styled from "styled-components";
import {BrowserRouter, NavLink} from "react-router-dom";

const MenuNav = styled.nav`
  ul {
    justify-content: center;
    display: flex;
    list-style: none;
    li {
        margin: 0 1rem;
    }
  }
`

function App() {
  return (
    <div className="App">
      <header role='contentinfo' className="App-header"/>
      <main>
        <h1>Les web workers</h1>
          <BrowserRouter>
              <MenuNav role='navigation' aria-label='Menu principal'>
                  <ul>
                        <li><NavLink to="/">Simple worker</NavLink></li>
                        <li><NavLink to="/dedicated-worker">Dedicated worker</NavLink></li>
                        <li><NavLink to="/shared-worker">Shared worker</NavLink></li>
                        <li><NavLink to="/worklet">Paint Worklet</NavLink></li>
                        <li><NavLink to="/scenario">Worker works in background</NavLink></li>
                  </ul>
              </MenuNav>
            <Router />
          </BrowserRouter>
      </main>
    </div>
  );
}

export default App;
