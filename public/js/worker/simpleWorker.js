// If you need to import a library, you can use importScripts
// importScripts('https://unpkg.com/axios/dist/axios.min.js')

function sleep (ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

onmessage = (event) => {
    console.log('Start Simple Worker', event)
    console.log('on message on worker', event)
    sleep(2000).then(() => {
        console.log('post message on worker', event.data)
        const reverseMessage = event.data.split("").reverse().join("");
        postMessage(reverseMessage)
        console.log('End Simple Worker', event)
    })
}
